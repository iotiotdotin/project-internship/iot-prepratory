# Module 0 - Skilling prerequisites

Before you begin your journey with us, this module will make sure you have the
prerequisites for the Skilling Program.

#### What you will you learn?
1. Basics of C Programming (IoT)
2. Basics of C++ Programming (AI-CPP)

## How to complete the Module?
Learning a skill happens in 3 stages Understand, Summarize & Practice (USP). This applies to the module as well.

| Process Stage | Description | 
|---------------|-----------|
| Understand    | Read course material and try to understand topics covered in the Module.           |
| Summarize (Assignment)     | Reinforce critical parts of the subject by writing them down in concise Summaries. |
| Practice  (Assignment)    | Practice skills, to make your learning. This where your brain learns the most.     |
| Submit Assignments    | Submit assignments for grading and feedback from the mentors. |

### Navigating in Gitlab 

1. Understand Stage: Find the Reading Material for the Module in the Module Wiki.
2. Summarize Assignments: Find the Summarize Assignments in the Module Issues.
3. Practice Assignments: Find the Practice Assignments in the Module Issues. 
4. Submitting Assignments: Find the Issue to submit Assignments in the Module Issues.

![Gitlab pointers](/extras/01.png)


## Asking Questions/Queries about the Module
Ask your questions in the respective Module Issues

![Gitlab pointers](/extras/03.png)

